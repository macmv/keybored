#include <windows.h>
#include <stdlib.h>
#include <stdio.h>

// Copied from https://gist.github.com/proprietary/0a23fd89d6772f96acde8196b730c649

int main(int argc, char* argv[]) {
  FILTERKEYS keys{ sizeof(FILTERKEYS) };

  keys.dwFlags = FKF_FILTERKEYSON | FKF_AVAILABLE;
  keys.iDelayMSec = 150;
  keys.iRepeatMSec = 1000 / 50;

  if (!SystemParametersInfo(SPI_SETFILTERKEYS, sizeof(FILTERKEYS), (LPVOID)&keys, 0)) {
    fprintf(stderr, "System call failed.\nUnable to set keyrate.");
    return 1;
  }
  printf("delay: %d, rate: %d\n", (int)keys.iDelayMSec, (int)keys.iRepeatMSec);

  return 0;
}